# tester-matching-backend

# Run

1. Clone project
2. Enter root directory of the project via Comannd Line
3. Start the application by typing "mvn spring-boot:run"
4. Make sure that front-end part of the application is also running

# Testing API without front-end part

Here are example URLs for data retrieval via API endpoints

All countries:

localhost:8080/countries

All devices:

localhost:8080/devices

Tester matches endpoint:

localhost:8080/search-tester-device-bug-count?countryList=US&countryList=GB&countryList=JP&deviceIdList=2&deviceIdList=3&deviceIdList=4&deviceIdList=5&deviceIdList=6&deviceIdList=7&deviceIdList=8&deviceIdList=9&deviceIdList=10