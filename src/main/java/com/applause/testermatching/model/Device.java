package com.applause.testermatching.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class Device {

    @Id
    private Long deviceId;

    @Column(unique = true)
    @Length(max = 30)
    private String description;

    @ManyToMany(mappedBy = "devices")
    private Set<Tester> testers;

}
