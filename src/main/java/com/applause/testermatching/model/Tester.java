package com.applause.testermatching.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class Tester {

    @Id
    private Long testerId;

    @Column
    @Length(max = 30)
    private String firstName;

    @Column
    @Length(max = 30)
    private String lastName;

    @Column
    @Length(max = 30)
    private String country;

    @Column
    private Timestamp lastLogin;

    @ManyToMany
    @JoinTable(
            name = "tester_device",
            joinColumns = @JoinColumn(name = "testerId"),
            inverseJoinColumns = @JoinColumn(name = "deviceId"))
    private Set<Device> devices;
}
