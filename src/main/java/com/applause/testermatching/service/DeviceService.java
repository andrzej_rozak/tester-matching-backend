package com.applause.testermatching.service;

import com.applause.testermatching.dto.DeviceDto;

import java.util.List;

public interface DeviceService {
    List<DeviceDto> getAllDevices();
}
