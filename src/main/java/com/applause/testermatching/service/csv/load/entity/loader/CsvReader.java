package com.applause.testermatching.service.csv.load.entity.loader;

import com.opencsv.bean.CsvToBeanBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class CsvReader {

    @Autowired
    protected ResourceLoader resourceLoader;

    protected <T> List<T> loadObjectListFromCsv(Class<T> clazz, String path) {
        log.info("Loading " + clazz.getSimpleName() + " has started");
        List<T> list;

        try (Reader reader = new FileReader(resourceLoader.getResource(path).getFile())) {
            CsvToBeanBuilder<T> csvToBeanBuilder = new CsvToBeanBuilder<>(reader);
            list = csvToBeanBuilder
                    .withType(clazz)
                    .build()
                    .parse();
        } catch (IOException e) {
            log.error("Error while loading " + clazz.getSimpleName() + " records", e);
            list = new ArrayList<>();
        }
        log.info("Loading " + clazz.getSimpleName() + " has finished");
        return list;
    }
}
