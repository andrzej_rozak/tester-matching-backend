package com.applause.testermatching.service.csv.load;

import com.applause.testermatching.model.Bug;
import com.applause.testermatching.model.Device;
import com.applause.testermatching.model.Tester;
import com.applause.testermatching.service.csv.load.entity.loader.BugCsvReader;
import com.applause.testermatching.service.csv.load.entity.loader.DeviceCsvReader;
import com.applause.testermatching.service.csv.load.entity.loader.TesterCsvReader;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Service
public class CsvDataLoader {

    private final BugCsvReader bugCsvReader;
    private final DeviceCsvReader deviceCsvReader;
    private final TesterCsvReader testerCsvReader;

    public List<Device> loadDevices() {
        return deviceCsvReader.load();
    }

    public List<Tester> loadTesters() {
        return testerCsvReader.load();
    }

    public List<Bug> loadBugs(List<Tester> testerList, List<Device> deviceList) {
        return bugCsvReader.load(testerList, deviceList);
    }
}
