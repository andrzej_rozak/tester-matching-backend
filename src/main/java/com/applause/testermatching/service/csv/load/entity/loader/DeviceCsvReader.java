package com.applause.testermatching.service.csv.load.entity.loader;

import com.applause.testermatching.model.Device;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component
public class DeviceCsvReader extends CsvReader {

    private final String DEVICES_CSV_DIR = "classpath:csv/devices.csv";

    public List<Device> load() {
        return loadObjectListFromCsv(Device.class, DEVICES_CSV_DIR);
    }
}
