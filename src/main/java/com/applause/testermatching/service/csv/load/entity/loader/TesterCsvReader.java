package com.applause.testermatching.service.csv.load.entity.loader;

import com.applause.testermatching.model.Tester;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component
public class TesterCsvReader extends CsvReader {

    private final String TESTERS_CSV_DIR = "classpath:csv/testers.csv";

    public List<Tester> load() {
        return loadObjectListFromCsv(Tester.class, TESTERS_CSV_DIR);
    }
}
