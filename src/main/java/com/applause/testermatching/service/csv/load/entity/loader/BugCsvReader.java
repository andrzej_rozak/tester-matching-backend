package com.applause.testermatching.service.csv.load.entity.loader;

import com.applause.testermatching.dto.BugCsvDto;
import com.applause.testermatching.model.Bug;
import com.applause.testermatching.model.Device;
import com.applause.testermatching.model.Tester;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Component
public class BugCsvReader extends CsvReader {

    private final String BUGS_CSV_DIR = "classpath:csv/bugs.csv";

    public List<Bug> load(List<Tester> testerList, List<Device> deviceList) {
        List<BugCsvDto> bugCSVList = loadObjectListFromCsv(BugCsvDto.class, BUGS_CSV_DIR);
        List<Bug> bugList = new ArrayList<>();

        bugCSVList.forEach(bugCsv ->
                bugList.add(
                        Bug.builder()
                                .bugId(bugCsv.getBugId())
                                .device(deviceList.stream()
                                        .filter(device -> device.getDeviceId().equals((bugCsv.getDeviceId())))
                                        .findFirst().orElse(null))
                                .tester(testerList.stream()
                                        .filter(tester -> tester.getTesterId().equals(bugCsv.getTesterId()))
                                        .findFirst().orElse(null))
                                .build()
                )
        );

        return bugList.stream()
                .filter(bug -> bug.getDevice() != null && bug.getTester() != null)
                .collect(Collectors.toList());
    }
}
