package com.applause.testermatching.service;

import com.applause.testermatching.dto.TesterDeviceBugCountDto;
import com.applause.testermatching.dto.TesterMatchSearchCriteriaDto;

import java.util.List;

public interface TesterMatchingService {
    List<TesterDeviceBugCountDto> getTesterDeviceBugCount(TesterMatchSearchCriteriaDto testerMatchSearchCriteriaDto);
}
