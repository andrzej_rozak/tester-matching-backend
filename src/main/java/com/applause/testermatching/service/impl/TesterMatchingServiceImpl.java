package com.applause.testermatching.service.impl;

import com.applause.testermatching.dto.TesterDeviceBugCountDto;
import com.applause.testermatching.dto.TesterMatchSearchCriteriaDto;
import com.applause.testermatching.model.Device;
import com.applause.testermatching.model.Tester;
import com.applause.testermatching.repository.BugRepository;
import com.applause.testermatching.repository.DeviceRepository;
import com.applause.testermatching.repository.TesterRepository;
import com.applause.testermatching.service.TesterMatchingService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
@Service
public class TesterMatchingServiceImpl implements TesterMatchingService {

    private final TesterRepository testerRepository;

    private final DeviceRepository deviceRepository;

    private final BugRepository bugRepository;

    private final ModelMapper modelMapper;

    public List<TesterDeviceBugCountDto> getTesterDeviceBugCount(TesterMatchSearchCriteriaDto testerMatchSearchCriteriaDto) {
        List<Tester> testerList = testerRepository.findAllByCountryIn(testerMatchSearchCriteriaDto.getCountryList());
        List<Device> deviceList = deviceRepository.findAllByDeviceIdIn(testerMatchSearchCriteriaDto.getDeviceIdList());

        return bugRepository.findTBugsCountPerTesterAndDevice(testerList, deviceList).stream()
                .map(testerDeviceBugCountDto -> modelMapper.map(testerDeviceBugCountDto, TesterDeviceBugCountDto.class))
                .collect(Collectors.toList());
    }
}
