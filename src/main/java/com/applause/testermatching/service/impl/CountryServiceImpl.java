package com.applause.testermatching.service.impl;

import com.applause.testermatching.dto.CountryDto;
import com.applause.testermatching.dto.DeviceDto;
import com.applause.testermatching.dto.TesterDeviceBugCountDto;
import com.applause.testermatching.dto.TesterMatchSearchCriteriaDto;
import com.applause.testermatching.model.Device;
import com.applause.testermatching.model.Tester;
import com.applause.testermatching.repository.BugRepository;
import com.applause.testermatching.repository.DeviceRepository;
import com.applause.testermatching.repository.TesterRepository;
import com.applause.testermatching.service.CountryService;
import com.applause.testermatching.service.DeviceService;
import com.applause.testermatching.service.TesterMatchingService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
@Service
public class CountryServiceImpl implements CountryService {

    private final TesterRepository testerRepository;

    public Set<CountryDto> getAllCountries() {
        return testerRepository.selectDistinctCountries().stream()
                .map(CountryDto::new)
                .collect(Collectors.toSet());
    }
}
