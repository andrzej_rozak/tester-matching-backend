package com.applause.testermatching.service;

import com.applause.testermatching.dto.CountryDto;
import com.applause.testermatching.dto.DeviceDto;
import com.applause.testermatching.dto.TesterDeviceBugCountDto;
import com.applause.testermatching.dto.TesterMatchSearchCriteriaDto;

import java.util.List;
import java.util.Set;

public interface CountryService {
    Set<CountryDto> getAllCountries();
}
