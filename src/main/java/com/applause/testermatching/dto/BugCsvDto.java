package com.applause.testermatching.dto;

import lombok.Data;

@Data
public class BugCsvDto {
    private Long bugId;
    private Long deviceId;
    private Long testerId;
}