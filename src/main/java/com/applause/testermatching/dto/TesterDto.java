package com.applause.testermatching.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
public class TesterDto {
    Long testerId;
    String firstName;
    String lastName;
    String country;
    Date lastLogin;
}
