package com.applause.testermatching.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class DeviceDto {
    Long deviceId;
    String description;
}
