package com.applause.testermatching.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TesterDeviceBugCountDto {
    private TesterDto tester;
    private DeviceDto device;
    private Long bugCount;
    private Long experience;
}
