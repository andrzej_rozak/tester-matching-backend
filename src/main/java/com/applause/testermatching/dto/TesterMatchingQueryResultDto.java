package com.applause.testermatching.dto;

import com.applause.testermatching.model.Device;
import com.applause.testermatching.model.Tester;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TesterMatchingQueryResultDto {
    private Tester tester;
    private Device device;
    private Long bugCount;
    private Long experience;
}
