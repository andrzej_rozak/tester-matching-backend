package com.applause.testermatching.controller;

import com.applause.testermatching.dto.CountryDto;
import com.applause.testermatching.dto.DeviceDto;
import com.applause.testermatching.dto.TesterDeviceBugCountDto;
import com.applause.testermatching.dto.TesterMatchSearchCriteriaDto;
import com.applause.testermatching.service.CountryService;
import com.applause.testermatching.service.DeviceService;
import com.applause.testermatching.service.TesterMatchingService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Set;

@CrossOrigin(origins = "http://localhost:4200")
@RequiredArgsConstructor
@RestController
public class TesterMatchingController {

    private final TesterMatchingService testerMatchingService;
    private final DeviceService deviceService;
    private final CountryService countryService;

    @GetMapping("/countries")
    Set<CountryDto> getAllCountries() { return countryService.getAllCountries(); }

    @GetMapping("/devices")
    List<DeviceDto> getAllDevices() {
        return deviceService.getAllDevices();
    }

    @GetMapping("/search-tester-device-bug-count")
    List<TesterDeviceBugCountDto> searchTesterDeviceBugCount(TesterMatchSearchCriteriaDto testerMatchSearchCriteriaDto) {
        return testerMatchingService.getTesterDeviceBugCount(testerMatchSearchCriteriaDto);
    }
}
