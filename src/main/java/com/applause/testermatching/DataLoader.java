package com.applause.testermatching;

import com.applause.testermatching.model.Bug;
import com.applause.testermatching.model.Device;
import com.applause.testermatching.model.Tester;
import com.applause.testermatching.repository.BugRepository;
import com.applause.testermatching.repository.DeviceRepository;
import com.applause.testermatching.repository.TesterRepository;
import com.applause.testermatching.service.csv.load.CsvDataLoader;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


@Data
@Component
@RequiredArgsConstructor
@Profile("!test")
public class DataLoader implements ApplicationRunner {

    private final TesterRepository testerRepository;

    private final DeviceRepository deviceRepository;

    private final BugRepository bugRepository;

    private final CsvDataLoader csvDataLoader;

    public void run(ApplicationArguments args) {
        List<Tester> testerList = csvDataLoader.loadTesters();
        List<Device> deviceList = csvDataLoader.loadDevices();
        List<Bug> bugList = csvDataLoader.loadBugs(testerList, deviceList);

        testerList.forEach(tester -> tester.setDevices(filterDevicesForTester(tester, bugList)));

        deviceRepository.saveAll(deviceList);
        testerRepository.saveAll(testerList);
        bugRepository.saveAll(bugList);
    }

    private Set<Device> filterDevicesForTester(Tester tester, List<Bug> bugList) {
        List<Bug> testersBugs = bugList.stream()
                .filter(bug -> bug.getTester().getTesterId().equals(tester.getTesterId()))
                .collect(Collectors.toList());
        return testersBugs.stream().map(Bug::getDevice).collect(Collectors.toSet());
    }

}