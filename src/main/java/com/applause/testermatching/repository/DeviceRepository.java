package com.applause.testermatching.repository;


import com.applause.testermatching.model.Device;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface DeviceRepository extends CrudRepository<Device, Long> {
    List<Device> findAll();

    List<Device> findAllByDeviceIdIn(List<Long> idList);
}
