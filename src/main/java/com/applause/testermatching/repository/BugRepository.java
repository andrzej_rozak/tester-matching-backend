package com.applause.testermatching.repository;


import com.applause.testermatching.dto.TesterMatchingQueryResultDto;
import com.applause.testermatching.model.Bug;
import com.applause.testermatching.model.Device;
import com.applause.testermatching.model.Tester;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface BugRepository extends CrudRepository<Bug, Long> {

    @Query("SELECT " +
            "new com.applause.testermatching.dto.TesterMatchingQueryResultDto(b.tester, b.device, COUNT(b.bugId), (SELECT COUNT(b.bugId) FROM Bug b WHERE b.tester=t AND b.device IN ?2))" +
            "FROM Bug b " +
            "INNER JOIN b.tester t " +
            "GROUP BY b.tester, b.device " +
            "HAVING b.tester IN ?1 AND b.device IN ?2"
    )
    List<TesterMatchingQueryResultDto> findTBugsCountPerTesterAndDevice(List<Tester> testerList, List<Device> deviceList);
}
