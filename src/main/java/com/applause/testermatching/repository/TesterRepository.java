package com.applause.testermatching.repository;


import com.applause.testermatching.model.Tester;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Set;

public interface TesterRepository extends CrudRepository<Tester, Long> {
    @Query("SELECT country FROM Tester")
    Set<String> selectDistinctCountries();

    List<Tester> findAllByCountryIn(List<String> countryList);
}
