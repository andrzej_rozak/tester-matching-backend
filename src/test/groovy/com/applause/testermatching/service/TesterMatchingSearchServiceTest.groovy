package com.applause.testermatching.service

import com.applause.testermatching.TestBeanConfig
import com.applause.testermatching.TestData
import com.applause.testermatching.TesterMatchingApplication
import com.applause.testermatching.dto.DeviceDto
import com.applause.testermatching.repository.BugRepository
import com.applause.testermatching.repository.DeviceRepository
import com.applause.testermatching.repository.TesterRepository
import com.applause.testermatching.service.impl.TesterMatchingServiceImpl
import org.modelmapper.ModelMapper
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.annotation.Import
import org.springframework.test.context.ActiveProfiles
import spock.lang.Specification


@SpringBootTest(classes = TesterMatchingApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Import(TestBeanConfig)
@ActiveProfiles("test")
class TesterMatchingSearchServiceTest extends Specification {

    private final BugRepository bugRepository = Mock()

    private final DeviceRepository deviceRepository = Mock()

    private final TesterRepository testerRepository = Mock()

    private ModelMapper modelMapper = Mock()

    private TesterMatchingServiceImpl testerMatchingService = new TesterMatchingServiceImpl(
            testerRepository,
            deviceRepository,
            bugRepository,
            modelMapper
    )


    def "should call repository method once and return all countries - the same as repository method returns"() {
        given:
        Set<String> allCountries = ["US", "GB", "PL"]

        when: "calling service method"
        Set<String> countries = testerMatchingService.getAllCountries()

        then: "repository method should be called once and return all countries"
        1 * testerRepository.selectDistinctCountries() >> allCountries
        countries == allCountries
    }

    def "should call repository method once and return all devices (including non tested devices) - same as repository method returns"() {
        given:
        List<DeviceDto> allDevices = [TestData.DEVICE_A, TestData.DEVICE_B, TestData.NOT_TESTED_DEVICE].collect {
            modelMapper.map(it, DeviceDto.class)
        }

        when: "calling service method"
        List<DeviceDto> devices = testerMatchingService.getAllDevices()

        then: "repository method should be called once and return all countries (including non tested)"
        1 * deviceRepository.findAll() >> allDevices
        allDevices == devices
    }

}
