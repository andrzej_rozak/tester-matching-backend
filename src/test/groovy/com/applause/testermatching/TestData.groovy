package com.applause.testermatching

import com.applause.testermatching.model.Bug
import com.applause.testermatching.model.Device
import com.applause.testermatching.model.Tester

import java.sql.Timestamp

class TestData {
    public static Tester TESTER_A = Tester.builder()
            .testerId(1)
            .country("US")
            .firstName("John")
            .lastName("Doe")
            .lastLogin(new Timestamp(new Date().getTime()))
            .build()

    public static Tester TESTER_B = Tester.builder()
            .testerId(2)
            .country("PL")
            .firstName("Jak")
            .lastName("Kowalski")
            .lastLogin(new Timestamp(new Date().getTime()))
            .build()

    public static Tester TESTER_C = Tester.builder()
            .testerId(3)
            .country("JP")
            .firstName("Tom")
            .lastName("House")
            .lastLogin(new Timestamp(new Date().getTime()))
            .build()

    public static Tester TESTER_D = Tester.builder()
            .testerId(4)
            .country("JP")
            .firstName("Sarah")
            .lastName("House")
            .lastLogin(new Timestamp(new Date().getTime()))
            .build()

    public static Device DEVICE_A = Device.builder()
            .deviceId(1)
            .description("iPhone 4")
            .build()

    public static Device DEVICE_B = Device.builder()
            .deviceId(2)
            .description("Galaxy S3")
            .build()

    public static Device NOT_TESTED_DEVICE = Device.builder()
            .deviceId(3)
            .description("HTC One X")
            .build()

    public static Bug BUG_A = Bug.builder()
            .bugId(1)
            .tester(TESTER_A)
            .device(DEVICE_A)
            .build()

    public static Bug BUG_B = Bug.builder()
            .bugId(2)
            .tester(TESTER_A)
            .device(DEVICE_A)
            .build()

    public static Bug BUG_C = Bug.builder()
            .bugId(3)
            .tester(TESTER_B)
            .device(DEVICE_A)
            .build()

    public static Bug BUG_D = Bug.builder()
            .bugId(4)
            .tester(TESTER_B)
            .device(DEVICE_B)
            .build()

    public static Bug BUG_E = Bug.builder()
            .bugId(4)
            .tester(TESTER_B)
            .device(DEVICE_B)
            .build()
}