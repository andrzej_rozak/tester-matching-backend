package com.applause.testermatching

import org.modelmapper.ModelMapper
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean

@TestConfiguration
class TestBeanConfig {

    @Bean
    ModelMapper modelMapper() {
        return new ModelMapper()
    }
}
