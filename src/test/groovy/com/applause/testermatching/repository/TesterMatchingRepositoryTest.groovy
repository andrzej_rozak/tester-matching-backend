package com.applause.testermatching.repository

import com.applause.testermatching.TestBeanConfig
import com.applause.testermatching.TestData
import com.applause.testermatching.TesterMatchingApplication
import com.applause.testermatching.dto.TesterMatchingQueryResultDto
import com.applause.testermatching.model.Device
import com.applause.testermatching.model.Tester
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment
import org.springframework.context.annotation.Import
import org.springframework.test.context.ActiveProfiles
import spock.lang.Specification

import javax.transaction.Transactional

@SpringBootTest(classes = TesterMatchingApplication.class, webEnvironment = WebEnvironment.RANDOM_PORT)
@Import(TestBeanConfig)
@ActiveProfiles("test")
class TesterMatchingRepositoryTest extends Specification {

    @Autowired
    private BugRepository bugRepository

    @Autowired
    private DeviceRepository deviceRepository

    @Autowired
    private TesterRepository testerRepository

    @Transactional
    def "bug repository should return testers and devices sorted by experience, without devices which haven't been tested by any tester"() {
        given:
        testerRepository.saveAll(Arrays.asList(TestData.TESTER_A, TestData.TESTER_B, TestData.TESTER_C))
        deviceRepository.saveAll(Arrays.asList(TestData.DEVICE_A, TestData.DEVICE_B, TestData.NOT_TESTED_DEVICE))
        bugRepository.saveAll(Arrays.asList(TestData.BUG_A, TestData.BUG_B, TestData.BUG_C, TestData.BUG_D, TestData.BUG_E))

        when: "repository method is called"
        List<TesterMatchingQueryResultDto> result = bugRepository.findTBugsCountPerTesterAndDevice(
                Arrays.asList(TestData.TESTER_A, TestData.TESTER_B),
                Arrays.asList(TestData.DEVICE_A, TestData.DEVICE_B, TestData.NOT_TESTED_DEVICE)
        )

        then: "most experienced tester should be on top of the list and NOT_TESTED_DEVICE should not be included"
        result.get(0).tester.testerId == 1
        result.findAll { it.device == TestData.NOT_TESTED_DEVICE }.size() == 0

    }

    @Transactional
    def "tester repository should return all countries with no duplicates"() {
        given:
        testerRepository.saveAll(Arrays.asList(TestData.TESTER_A, TestData.TESTER_B, TestData.TESTER_C, TestData.TESTER_D))
        Set<String> allCountries = ["US", "PL", "JP"]

        when: "repository method is called"
        Set<String> countries = testerRepository.selectDistinctCountries()

        then: "countries returned by repository methods should contain those in allCountry list"
        allCountries == countries
    }

    @Transactional
    def "device repository should return all devices"() {
        given:
        List<Device> allDevices = [TestData.DEVICE_A, TestData.DEVICE_B, TestData.NOT_TESTED_DEVICE]
        deviceRepository.saveAll(allDevices)

        when: "repository method is called"
        List<Device> devices = deviceRepository.findAll()

        then: "it should return all devices previously saved to database"
        allDevices == devices
    }

    @Transactional
    def "tester repository should return only testers from countries contained in list"() {
        given:
        testerRepository.saveAll(Arrays.asList(TestData.TESTER_A, TestData.TESTER_B, TestData.TESTER_C, TestData.TESTER_D))
        List<String> countries = ["US", "JP"]

        when: "repository method is called"
        List<Tester> testers = testerRepository.findAllByCountryIn(countries)

        then: "returned testers should only from countries in list"
        testers.collect { it.country }.every { countries.contains(it) }
    }
}
